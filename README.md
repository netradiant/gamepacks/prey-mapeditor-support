# Prey map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Prey.

This gamepack is based on the game pack from https://svn.icculus.org/gtkradiant-gamepacks/PreyPack
